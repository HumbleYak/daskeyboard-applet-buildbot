# Q Applet: Buildbot

Displays the build status of a build activity from a Buildbot server

[GitHub repository](https://HumbleYak@bitbucket.org/HumbleYak/daskeyboard-applet-buildbot.git)

## Example

Displays the build status of a build activity from a Buildbot server on a Das Keyboard Q Series.
Each color corresponds to a build status:  
pass (green), warning: (orange), fail (red), skipped (white), exception/retry (purple), cancelled (magenta), inprogress (yellow).

![Buildbot status on a Das Keybaord Q](assets/image_keys.png "Q Buildbot")
![Buildbot status on a Das Keybaord Q](assets/image_legend.png "Q Buildbot")

## Changelog

[CHANGELOG.MD](CHANGELOG.md)

## Installation

Requires a Das Keyboard Q Series: www.daskeyboard.com

Installation, configuration and uninstallation of applets is done within the Q Desktop application (<https://www.daskeyboard.com/q>)

## Running tests

- `yarn test`

## Contributions

Pull requests welcome.

## Copyright / License

Copyright 2014 - 2019 Das Keyboard / Metadot Corp.

Licensed under the GNU General Public License Version 2.0 (or later);
you may not use this work except in compliance with the License.
You may obtain a copy of the License in the LICENSE file, or at:

   <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
