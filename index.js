const q = require('daskeyboard-applet');
const fs = require('fs');
const request = require('request-promise');
const logger = q.logger;

var zones = null;

const STATUS = Object.freeze({
  PASS: 0,
  WARNING: 1,
  FAIL: 2,
  SKIPPED: 3,
  EXCEPTION: 4,
  RETRY: 5,
  CANCELLED: 6
})

const COLORS = Object.freeze({
  PASS: '#00FF00',
  WARNING: '#FFA500',
  FAIL: '#FF0000',
  SKIPPED: '#FFFFFF',
  EXCEPTION: '#800080',
  RETRY: '#800080',
  CANCELLED: '#FF0090',
  INPROGRESS: '#FFFF00'
})

const percentChanceExpression = /(\d+) percent/;

// this is a work-around to a bug in the API that seems to send stale status
function generateServiceHeaders() {
  return {
    'User-Agent': 'request(q-applet-buildbot)',
    'Accept': `application/geo+json, application/qawf-${new Date().getTime() + '' + Math.round(Math.random() * 10000)}`,
  };
}

async function getBuildstatus(builderId, buildbotHostId) {
  const url = `${buildbotHostId}/api/v2/builders/${builderId}/builds`;
  logger.info("Getting build status via URL: " + url);
  return request.get({
    url: url,
    headers: generateServiceHeaders(),
    json: true
  }).then(body => {
    const builds = body.builds;
    if(builds) {
      return body;
    } else {
      throw new Error("No list of builds returned.");
    }
  }).catch((error) => {
    logger.error(`Error when trying to get build status: ${error}`);
    throw new Error(`Error when trying to get build status: ${error}`);
  })
}

function generateText(build, builderLabel, buildbotHostId) {
  const output = [];

  output.push(`<div><em>${builderLabel}</em> [${build.builderid}] ${build.number}</div>`);
  
  let start = new Date(build.started_at*1000);
  let end = new Date(build.complete_at*1000);
  output.push(`<div><em>Start:</em> ${start.toLocaleString()}</div>`);
  output.push(`<div><em>End:</em> ${end.toLocaleString()}</div>`);
  output.push(`<div><em>Status:</em> ${build.state_string}</div>`);
  
  let url = `${buildbotHostId}/#/builders/${build.builderid}/builds/${build.number}`;
  output.push(`<div><a href=${url}>${url}</a></div>`);

  return output.join("\n");
}

class BuildbotStatus extends q.DesktopApp {
  constructor() {
    super();
    // run every min
    this.pollingInterval = 1 * 60 * 1000;
  }

  async options(fieldId, search) {
  }

  async applyConfig() {
    this.apiKey = this.authorization.apiKey;
  }

  async run() {
    logger.info("BuildbotStatus running.");
    
    logger.info(this.config.buildbotHostId);
    const builderLabel = this.config.builderLabel;
    const builderId = this.config.builderId;
    const buildbotHostId = this.config.buildbotHostId;
    if (builderId && buildbotHostId) {
      logger.info(`My builder ID is  : ${builderId}`);

      return getBuildstatus(builderId, buildbotHostId).then(body => {
        const builds = body.builds;
        if(builds == null) {
          logger.error("builds is null");
          return;
        }

        if(builds.length == 0) {
          logger.error("builds.length == 0");
          return;
        }

        const build = builds[builds.length - 1];
        if(build == null) {
          logger.error("build is null");
          return;
        }

        const points = [];

        var color;
        if(build.complete == false) {
          color = COLORS.INPROGRESS;
        } else if(build.results == STATUS.FAIL) {
          color = COLORS.FAIL;
        } else if(build.results == STATUS.WARNING) {
          color = COLORS.WARNING;
        } else if(build.results == STATUS.PASS) {
          color = COLORS.PASS;
        } else if(build.results == STATUS.EXCEPTION) {
          color = COLORS.EXCEPTION;
        } else if(build.results == STATUS.RETRY) {
          color = COLORS.RETRY;
        } else if(build.results == STATUS.CANCELLED) {
          color = COLORS.CANCELLED;
        } else {
          logger.error("Did not regonize the current built results");
          return;
        }
        points.push(new q.Point(color));

        const signal = new q.Signal({
          points: [points],
          name: `Buildbot Builder ${builderId}`,
          message: `<div><b>Build status for ${builderId}:</b></div>` +
            generateText(build, builderLabel, buildbotHostId)
        });
        logger.info('Sending signal: ' + JSON.stringify(signal));
        return signal;
      }).catch((error) => {
        logger.error(`Error while getting build status data: ${error}`);
          if(`${error.message}`.includes("getaddrinfo")){
            // Commented in order to do not send this kind of notification. Boring to close the signal.
            // return q.Signal.error(
            //   'The buildbot service returned an error. <b>Please check your internet connection</b>.'
            // );
          }else{
            return q.Signal.error([`The buildbot service returned an error. Detail: ${error}`]);
          }
      })
    } else {
      logger.info("No builderId configured.");
      return null;
    }
  }
}


module.exports = {
  BuildbotStatus: BuildbotStatus,
  generateServiceHeaders: generateServiceHeaders,
  generateText: generateText
}

const applet = new BuildbotStatus();